#pragma once

#define MSG_OK 0

class TDataCom {
protected:

    int _Message;

public:

    TDataCom() : _Message(MSG_OK) {}

    void SetMessage(int message) {
        _Message = message;
    }

    int GetMessage() {
        int m = _Message;
        _Message = MSG_OK;
        return m;
    }

};