#include <iostream>
#include "../include/TTable.h"
#include "../include/TSortTable.h"

using namespace std;

void PrintTable(TTable* table);

int main() {
    TSortTable table;
    table.InsRecord("a", 0);
    table.InsRecord("c", 0);
    table.InsRecord("b", 0);
    table.InsRecord("e", 0);
    table.InsRecord("d", 0);
    PrintTable(&table);

    getchar();
    return 0;
}


void PrintTable(TTable* table) {
    table->Reset();
    while (!table->IsTabEnded()) {
        std::cout << table->GetKey();
        table->GoNext();
    }
}
