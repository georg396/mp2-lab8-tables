#include "../include/TArrayTable.h"

TArrayTable::TArrayTable(int Size) {
    TabSize = Size;
    CurrPos = 0;
    DataCount = 0;
    pRecs = new PTTabRecord[Size];
    for (int i = 0; i < Size; i++)
        pRecs[i] = 0;
}

int TArrayTable::IsFull() const {
    return DataCount == TabSize;
}

int TArrayTable::GetTabSize() const {
    return DataCount;
}

TKey TArrayTable::GetKey(void) const {
    return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayTable::GetValuePTR(void) const {
    return pRecs[CurrPos]->GetValuePTR();
}

TKey TArrayTable::GetKey(TDataPos mode) const {
    switch (mode) {
    case FIRST_POS:
        return pRecs[0]->GetKey();
    case CURRENT_POS:
        return GetKey();
    case LAST_POS:
        return pRecs[DataCount - 1]->GetKey();
    }
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) const {
    switch (mode) {
    case FIRST_POS:
        return pRecs[0]->GetValuePTR();
    case CURRENT_POS:
        return GetValuePTR();
    case LAST_POS:
        return pRecs[DataCount - 1]->GetValuePTR();
    }
}

int TArrayTable::Reset(void) {
    CurrPos = 0;
    return CurrPos;
}

int TArrayTable::IsTabEnded(void) const {
    return CurrPos == DataCount;
}

int TArrayTable::GoNext(void) {
    if (CurrPos < DataCount) {
        CurrPos++;
        return 0;
    }
    else
        return 1;
}

int TArrayTable::SetCurrentPos(int pos) {
    CurrPos = pos;
    return CurrPos;
}

int TArrayTable::GetCurrentPos(void) const {
    return CurrPos;
}